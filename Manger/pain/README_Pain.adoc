= Résilience -> Manger -> pain
Bernard Mayer
v1.2, 2020-08-28
:toc: preamble

:description: Je ne sait pas encore ce \
    que je vais écrire ici...

Retour au document central link:./README.adoc[]

== Ce qui a trait au pain

=== Recette n°1
link:./pain/pain-001.adoc[recette pain à la cocotte n°1]

=== Recette n°2
link:./pain/Pain_002-1CocotteUstensiles.pdf[Pain_002-1CocotteUstensiles.pdf] +
link:./pain/Pain_002-2CocotteLevain.pdf[Pain_002-2CocotteLevain.pdf] +
link:./pain/Pain_002-3CocotteEtapesFabrication.pdf[Pain_002-3CocotteEtapesFabrication.pdf]

== Recette n°3
link:./pain/PainCocotte-003.pdf[PainCocotte-003.pdf]

== Recette n°4
link:./pain/PainCocotte-004.pdf[PainCocotte-004.pdf]

=== Document savant sur la panification
link:./pain/LaPanificationAuLevainNaturel_PhilippeRoussel.epub[Un ePub sur la panification]


