= Resilience / Lire
Bernard Mayer
v1.2, 2024-11-24
:toc-title: Table des matières
:toc: preamble
//:imagesdir: ../img

:ldquo: &laquo;
:rdquo: &raquo;

:description: Je ne sait pas encore ce \
    que je vais écrire ici...
    
// ---------------------------------------------------

- Brouillon : link:ALire.adoc[]
//- Notes géopolitique : link:Notes_geopolitique.adoc[]

- Un répertoire pour link:Lire/FichesDeLecture/[*les notes* et les fiches de lecture]
- Un répertoire pour link:Lire/LFSdir/[les ouvrages et documents] :



